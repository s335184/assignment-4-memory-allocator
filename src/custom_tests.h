//
// Created by HP on 12.03.2023.
//

#ifndef MEMORY_ALLOCATOR_CUSTOM_TESTS_H
#define MEMORY_ALLOCATOR_CUSTOM_TESTS_H
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdint.h>

void run_all_tests();

#endif //MEMORY_ALLOCATOR_CUSTOM_TESTS_H
