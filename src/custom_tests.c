#include "custom_tests.h"

#define HEAP_SIZE 4096
#define LINE(t) printf("====== TEST %d =====\n", t);
void *heap = NULL;

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void kill_heap(void *heap_to_kill) {
    munmap(heap_to_kill, HEAP_SIZE);
}

void test_0_heap_creation() {
    heap = heap_init(HEAP_SIZE);
    if (heap == NULL) {
        kill_heap(heap);
        err("Failed test 0: heap initialization");
    }
    printf("Test 0: success\n");
    debug_heap(stdout, heap);
}

void test_1_malloc() {
    void *allocated = _malloc(2048);
    if (allocated == NULL) {
        debug_heap(stderr, heap);
        err("Failed test 1: _malloc()");
    }
    printf("Test 1: success\n");

    puts("Heap before free");
    debug_heap(stdout, heap);
    _free(allocated);

    puts("Heap after free");
    debug_heap(stdout, heap);
}

void test_2_free_one_region() {
    void *allocated0 = _malloc(1024);
    void *allocated1 = _malloc(1024);
    void *allocated2 = _malloc(1024);
    if (allocated0 == NULL || allocated1 == NULL || allocated2 == NULL) {
        debug_heap(stderr, heap);
        kill_heap(heap);
        err("Failed test 2: _malloc()");
    }
    debug_heap(stdout, heap);
    _free(allocated0);
    _free(allocated1);
    _free(allocated2);
    debug_heap(stdout, heap);
}

void test_3_mem_ended() {
    void *allocated0 = _malloc(4096);
    void *allocated1 = _malloc(4096);
    struct block_header *header0 = block_get_header(allocated0);
    struct block_header *header1 = block_get_header(allocated1);
    if (header0->next != header1) {
        kill_heap(heap);
        err("Headers are not linking to each other");
    }
    _free(allocated0);
    _free(allocated1);
    printf("Test 3: success\n");
}

void test_4_several_blocks() {
    void *allocated0 = _malloc(1000);
    struct block_header *block_1 = block_get_header(allocated0);
    debug_heap(stdout, block_1);
    void *allocated1 = _malloc(10000);
    void *allocated2 = _malloc(100);
    debug_heap(stdout, block_1);
    if (allocated1 == NULL) {
        kill_heap(heap);
        err("Failed test: 4");
    }

    printf("Test 4: success\n");

    _free(allocated0);
    _free(allocated1);
    _free(allocated2);
}

void test_5_another_heap() {
    heap_init(1);
    void *allocated0 = _malloc(12000);
    struct block_header *header = block_get_header(allocated0);
    debug_heap(stdout, header);

    struct block_header *next_block = header->next;
    void *offset = next_block->contents + next_block->capacity.bytes;
    void *mmaped = mmap(offset, 256, PROT_READ | PROT_WRITE, MAP_PRIVATE | 0x20, -1, 0);

    if (mmaped == NULL) {
        kill_heap(heap);
        err("Failed test: 5");
    }
    void *allocated1 = _malloc(4000);

    debug_heap(stdout, header);
    struct block_header *block_2 = block_get_header(allocated1);

    if (!(header->is_free || block_2->is_free)) {
        _free(allocated0);
        _free(allocated1);
    }
    debug_heap(stdout, header);
    printf("Test 5: success\n");
}


void run_all_tests() {
    LINE(0)
    test_0_heap_creation();
    LINE(1)
    test_1_malloc();
    LINE(2)
    test_2_free_one_region();
    LINE(3)
    test_3_mem_ended();
    LINE(4)
    test_4_several_blocks();
    LINE(5)
    test_5_another_heap();
    kill_heap(heap);
}
